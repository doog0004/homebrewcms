<?php
/**
 * Order.php - renders an order form where the authenticated user can place an order
 * 
 * @author Bugslayer
 * 
 */
?>
<form name="input" action="?action=save&page=order" method="post"
	style="width: 850px; margin-left: auto; margin-right: auto">
	<h1>Bestelformulier</h1>
	<p>Gebruik dit formulier om uw Slevels, wigbekken en zwenkmoeren bij
		@@project te bestellen</p>
	<input type="hidden" name="send" value="true" />
	<table style="width:850px">
		<tr>
			<td><label for="slevels">Aantal slevels</label></td>
			<td><input type="number" id="slevels" name="slevels" size="3"
				maxlength="3" value="0"></td>
		</tr>
		<tr>
			<td><label for="wigbekken">Aantal wigbekken</label></td>
			<td><input type="number" id="wigbekken" name="wigbekken" size="3"
				maxlength="3" value="0"></td>
		</tr>
		<tr>
			<td><label for="zwenkmoeren">Aantal zwenkmoeren</label></td>
			<td><input type="number" id="zwenkmoeren" name="zwenkmoeren" size="3"
				maxlength="3" value="0"></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit"
				value="Verstuur"></td>
		</tr>
	</table>
</form>
